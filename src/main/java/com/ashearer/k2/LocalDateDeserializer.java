package com.ashearer.k2;

import java.io.IOException;
import java.time.LocalDate;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.deser.std.StdDeserializer;

// LocalDate deserializer, like JSR310Module or JavaTimeModule, but compatible
// with older versions of Jackson, as imported from org.codehaus.jackson.
// The JavaTimeModule in Jackson 2.7 & up isn't available due to an apparent dependency version
// mismatch with other modules.

public class LocalDateDeserializer extends StdDeserializer<LocalDate> {

    protected LocalDateDeserializer() {
        super(LocalDate.class);
    }

    @Override
    public LocalDate deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        return LocalDate.parse(jp.readValueAs(String.class));
    }
}
