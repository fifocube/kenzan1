package com.ashearer.k2;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
//import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import com.sun.jersey.spi.resource.Singleton;

@Path("/employees")
@Singleton
@Produces(MediaType.APPLICATION_JSON)
public class Employees {
    // Use TreeMap to track all employees, so that they remain deterministically ordered by ID
    // when retrieved. All access is from synchronized methods.
    private final Map<String, Employee> employees = new TreeMap<>();
    private final Set<String> validAuthTokens = new HashSet<>();

    public Employees() {
        ObjectMapper mapper = new ObjectMapper();
        
        /* For simpler Java 8 LocalDate field support, would do:
         * mapper.registerModule(new JavaTimeModule());
         * mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
         *
         * but this requires a recent (2.7+) version of Jackson, and we're currently using an older version of Jersey
         * with JSON support that's incompatible with it.
         * So instead, there are custom (de)serializers on date fields.
         */
    
        InputStream is = getClass().getResourceAsStream("/employees.json");
        try {
            for (Employee emp : mapper.readValue(is, Employee[].class)) {
                employees.put(emp.getId(), emp);
            }
        } catch (java.io.IOException e) {
                e.printStackTrace();
        } finally {
        }
        
        is = getClass().getResourceAsStream("/tokens.json");
        try {
                Collections.addAll(validAuthTokens, mapper.readValue(is,  String[].class));
        } catch (java.io.IOException e) {
                e.printStackTrace();
        }
    }

    @GET
    @Path("{empid}")
    @Produces(MediaType.APPLICATION_JSON)
    synchronized
    public Employee getEmployee(@PathParam("empid") String empid) {
        Employee emp = employees.get(empid);
        if (emp == null || !emp.isActive()) {
            // Only active employees should be returned.
            // Treat any other employee ID the same as an unknown employee.
            return null;
            // TODO
            /*throw new NotFoundException(Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Employee " + empid + " not found")
                    .build());
                    */
        }

        return emp;
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    synchronized
    public void putEmployee(Employee emp, @Context HttpServletRequest httpRequest) {
        checkAuthForUpdate(httpRequest);
        // TODO: fail if emp ID already exists & is active?
        employees.put(emp.getId(), emp);
    }
    
    @DELETE
    @Path("{empid}")
    synchronized
    public void deleteEmployee(@PathParam("empid") String empid, @Context HttpServletRequest httpRequest) {
        checkAuthForUpdate(httpRequest);
        Employee emp = employees.get(empid);
        // no-op if employee is missing or already inactive
        if (emp == null) {
            return;
        }
        emp.deactivate();
    }
    
    @POST
    @Path("{empid}")
    @Consumes(MediaType.APPLICATION_JSON)
    synchronized
    public void updateEmployee(@PathParam("empid") String empid, Employee newEmp, @Context HttpServletRequest httpRequest) {
        // Verify authentication
        checkAuthForUpdate(httpRequest);
        
        Employee oldEmp = employees.get(empid);
        if (oldEmp == null || !oldEmp.isActive()) {
            return; // TODO: 404 Not Found error
        }
        employees.put(empid, newEmp);
    }
    
    // Return all active employees, filtering out inactive employees
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    synchronized
    public Collection<Employee> getAllActiveEmployees() {
        return employees.values().stream()
                .filter(emp -> emp.isActive())
                .collect(Collectors.toList());
    }

    /**
     * This method ensures that the request has an authorization header.
     * (It could be replaced with method annotations that invoke a custom Jersey filter,
     * at the cost of a somewhat more complex implementation that differs between
     * Jersey 1 and 2.)
     * @param httpRequest
     */
    private void checkAuthForUpdate(HttpServletRequest httpRequest) {
        // Get the HTTP Authorization header from the request,
        // and ensure that it has the format of a Bearer token
        String authHeader =
            httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            //throw new NotAuthorizedException("Authorization header must be provided");
                throw new RuntimeException("Bearer authorization was not provided");
        }

        // Extract the token from the HTTP Authorization header
        String token = authHeader.substring("Bearer".length()).trim();

        try {
            // Validate the token
            validateToken(token);

        } catch (Exception e) {
            //httpRequest.abortWith(
            //    Response.status(Response.Status.UNAUTHORIZED).build());
            throw new RuntimeException("Unauthorized"); // TODO
        }
    }

    private void validateToken(String token) throws Exception {
        // Check if it was issued by the server and if it's not expired
        // Throw an Exception if the token is invalid
        if (!validAuthTokens.contains(token)) {
            throw new RuntimeException("Unauthorized");
        }
    }
}
