
package com.ashearer.k2;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.time.LocalDate;

public class Employee {
    /**
     * Unique identifier for an employee
     */
    private String id;
    /**
     * Employee first name
     */
    private String firstName;
    /**
     * Employee middle initial
     */
    private String middleInitial;
    /**
     * Employee last name
     */
    private String lastName;
    /**
     * Employee birthday and year
     */
    private LocalDate dateOfBirth;
    /**
     * Employee start date
     */
    private LocalDate dateOfEmployment;
    /**
     * Employee status, true for ACTIVE, or false for INACTIVE
     */
    private Boolean isActive = true;
    
    /**
     * Translation of isActive: true to Status string
     */
    public static final String ACTIVE_STATUS = "ACTIVE";
    /**
     * Translation of isActive: false to Status string
     */
    public static final String INACTIVE_STATUS = "INACTIVE";
    
    public Employee() {
    }
    public String getId() {
        return this.id;
    }
    public String getFirstName() {
        return this.firstName;
    }
    public String getMiddleInitial() {
        return this.middleInitial;
    }
    public String getLastName() {
        return this.lastName;
    }
    /**
     * Attach custom JSON Serializer and deserializer to use ISO date format for JSON dates.
     * Jackson version incompatibility with the older version of Jersey currently in use
     * interferes with using Jackson 2.7 and up's optional support for new Java 8 LocalDate class.
     * 
     * @return LocalDate
     */
    @JsonDeserialize(using = LocalDateDeserializer.class)  
    @JsonSerialize(using = LocalDateSerializer.class)  
    public LocalDate getDateOfBirth() {
        return this.dateOfBirth;
    }
    @JsonDeserialize(using = LocalDateDeserializer.class)  
    @JsonSerialize(using = LocalDateSerializer.class)  
    public LocalDate getDateOfEmployment() {
        return this.dateOfEmployment;
    }
    
    /**
     * Used internally by JSON API, when testing whether employee record should be visible,
     * and not exported to JSON results. Instead, JSON objects get a computed "status" field.
     * @return whether the employee is active
     */
    @JsonIgnore
    public boolean isActive() {
        return this.isActive;
    }
    /**
     * Computed "status" field, for JSON export.
     * @return status string
     */
    public String getStatus() {
        return this.isActive ? ACTIVE_STATUS : INACTIVE_STATUS;
    }
    /**
     * Mark the employee as inactive.
     */
    public void deactivate() {
        this.isActive = false;
    }
}
