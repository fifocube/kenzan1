package com.ashearer.k2;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.ser.std.SerializerBase;

// LocalDate serializer, like JSR310Module or JavaTimeModule, but compatible
// with older versions of Jackson, as imported from org.codehaus.jackson.

public class LocalDateSerializer extends SerializerBase<LocalDate> {
    protected LocalDateSerializer() {
        super(LocalDate.class, true);
    }
    @Override
    public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider sp) throws IOException, JsonProcessingException {
        gen.writeString(value.format(DateTimeFormatter.ISO_LOCAL_DATE));
    }
}
