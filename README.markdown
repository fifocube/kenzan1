# Installation

To run a test server, with Maven and JDK 1.8 installed, change the current directory to a repository checkout with "pom.xml" at the root, and enter into a command prompt:

`mvn clean install`

`mvn embedded-glassfish:run`

# Initial Data

The initial employee data is loaded from an array of JSON records in `src/main/resources/employees.json`.

--
# Employee record

The employee JSON record contains each of the fields specified in the requirements.

--
# REST API calls

All API calls, each headed by HTTP method and pathname. Parameters in the pathname are in curly brackets. For running on the test server, pathnames must be prefixed with `http://localhost:8080/k2/webresources`.

## Authentication

Authenticated API calls must include an `Authenticate: Bearer` header. A token must follow, which must match any of the tokens listed in `src/main/resources/tokens.json`.


## GET /employees

Request Body: none

Response Type: `application/json`

Response Body: a JSON array of Employee JSON records

Return a JSON array of employee records for all active employees. The format of each array element is the same as the JSON argument for the PUT command. Inactive employees are skipped.

To consider: Add "url" fields (or even send a list of these URLs rather than a list of employee records), specifying the URL to each employee. This would reduce the published interface to one endpoint, and fit more cleanly into the ideal REST model. However, it may not be as usable in common client development workflows, and the security implications of following arbitrary URLs should be carefully considered.

## GET /employees/{id}

Request Body: none

Response Type: `application/json`

Response Body: Employee record in JSON format, including an ID field.

## PUT /employees

Request Type: `application/json`

Request Body: Employee record in JSON format, including an "ID" field

Response Body: none

Create a new employee. Submit the employee record in the request body as type `application/json`, including an "ID" field. The Status field cannot be set by this call (only by the DELETE method). 

Returns an error if the employee ID is already taken by an active employee. If an inactive employee has the same ID, the record will be replaced.

Returns HTTP Status 200 OK on success, whether the record was newly created or replaced an inactive employee.

Authentication is required.


## POST /employees/{id}

Request Type: application/json

Request Body: Employee JSON record without ID

Response Body: none

Replace the employee record with ID given by the "{id}" path parameter. Send the new employee record in the body of the request as type `application/json`, following the same format as the PUT method, except that any "ID" field in the request body is ignored in favor of the one in the pathname. The ID in the path must match an active employee's ID, or this method will return an error.

Returns HTTP Status 200 OK on success.

Authentication is required.

## DELETE /employees/{id}

Request Body: None

Response Body: None

Marks the employee with the ID denoted by "{id}", if any, as inactive.
It's OK to call this more than once for the same record, or on a nonexistent or inactive record, which are all treated as a successful no-op.

Returns HTTP Status 200 OK on success, meaning that there is now no active employee by that ID.

Authentication is required.


--
Andrew Shearer
